import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class Task {
  final String title;
  final String description;
  final String date;

  const Task({
    required this.title,
    required this.description,
    required this.date,
  });
}

class _HomePageState extends State<HomePage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController desController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  List cardColorList = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
  ];

  List<Task> taskList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(5),
        child: ListView.builder(
          itemCount: taskList.length,
          itemBuilder: (context, index) {
            return Container(
              width: 420,
              height: 120,
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: cardColorList[index % cardColorList.length],
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        width: 52,
                        height: 52,
                        margin: const EdgeInsets.only(left: 8, top: 12),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: const Color.fromRGBO(255, 255, 255, 1),
                        ),
                        child: Image.asset(
                          "assets/images/img.png",
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              width: 243,
                              height: 15,
                              margin: const EdgeInsets.only(top: 5),
                              child: Text(
                                taskList[index].title,
                                style: GoogleFonts.quicksand(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              width: 243,
                              height: 44,
                              child: Text(
                                taskList[index].description,
                                style: GoogleFonts.quicksand(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      const SizedBox(width: 5),
                      Text(
                        taskList[index].date,
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () {
                          titleController.clear();
                          desController.clear();
                          dateController.clear();
                          _displayModalBottomSheet(editIndex: index);
                        },
                        child: const Icon(
                          Icons.edit_outlined,
                          color: Color.fromRGBO(0, 139, 148, 1),
                        ),
                      ),
                      const SizedBox(width: 10),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            taskList.removeAt(index);
                          });
                        },
                        child: const Icon(Icons.delete_outlined,
                            color: Color.fromRGBO(0, 139, 148, 1)),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
      floatingActionButton: SizedBox(
        height: 50,
        width: 100,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {
              titleController.clear();
              desController.clear();
              dateController.clear();
              _displayModalBottomSheet();
            },
            shape: ContinuousRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: const Icon(Icons.add_outlined),
          ),
        ),
      ),
    );
  }

  void _displayModalBottomSheet({int? editIndex}) {
    String titleOutput;
    String descriptionOutput;
    String dateOutput;

    if (editIndex != null) {
      titleController.text = taskList[editIndex].title;
      desController.text = taskList[editIndex].description;
      dateController.text = taskList[editIndex].date;
    }

    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: MediaQuery.of(context).viewInsets, // Way 2
          //Way 1:  EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),

          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
              ),
              // const SizedBox(height: 5),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Title",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  const SizedBox(height: 5),
                  TextField(
                    controller: titleController,
                    decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.purpleAccent),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        hintText: "Enter title"),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Description",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(0, 139, 148, 1),
                      fontWeight: FontWeight.w400,
                      fontSize: 11,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  TextField(
                    controller: desController,
                    decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(
                              0,
                              139,
                              148,
                              1,
                            ),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: Colors.purpleAccent,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        hintText: "Enter Description"),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Date",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(0, 139, 148, 1),
                      fontWeight: FontWeight.w400,
                      fontSize: 11,
                    ),
                  ),
                  const SizedBox(height: 5),
                  TextField(
                    controller: dateController,
                    readOnly: true,
                    decoration: InputDecoration(
                        suffixIcon: const Icon(Icons.date_range_rounded),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: Colors.purpleAccent,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        hintText: "Enter Date"),
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2024),
                        lastDate: DateTime(2025),
                      );

                      String formatedDate =
                          DateFormat.yMMMd().format(pickedDate!);
                      setState(() {
                        dateController.text = formatedDate;
                      });
                    },
                  ),
                ],
              ),

              const SizedBox(
                height: 20,
              ),

              Container(
                  height: 50,
                  width: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
                      ),
                      onPressed: () {
                        //Navigator.of(context).pop();   // Way to remove hide the bottomsheet
                        Navigator.pop(
                            context); // Alternate way to hide the bottomsheet
                        setState(() {
                          titleOutput = titleController.text;
                          descriptionOutput = desController.text;
                          dateOutput = dateController.text;

                          // if (titleOutput.isEmpty ||
                          //     descriptionOutput.isEmpty ||
                          //     dateOutput.isEmpty) {}

                          if (titleOutput.isNotEmpty &&
                              descriptionOutput.isNotEmpty &&
                              dateOutput.isNotEmpty) {
                            if (editIndex != null) {
                              taskList[editIndex] = Task(
                                title: titleOutput,
                                description: descriptionOutput,
                                date: dateOutput,
                              );
                            } else {
                              taskList.add(
                                Task(
                                  title: titleOutput,
                                  description: descriptionOutput,
                                  date: dateOutput,
                                ),
                              );
                            }
                          }
                        });
                      },
                      child: Text(
                        "Submit",
                        style: GoogleFonts.quicksand(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                      ))),
            ],
          ),
        );
      },
    );
  }
}
